# DroneDockerTest


 *   Créer un dépot Git dans gitea
 *   Ajouter fichier .drone.yml dans le dépot
 *   Onglet "Drone" puis "Sync"
 *   Choisir le repository git
 *   Onglet "settings" : "activate repository"
 *   Onglet "Secrets" : ajouter les secrets , db_root_password , db_database, db_user , db_password
 *   Puis cliquer sur "NEw Build" et "create" (ne rien ajouter)
 *   Cliquer sur ajouter ".drone.yml" :

## Supplément 
les bug fix:
- Dans le Yaml attention au variable qu'il faut déclarer dans les secrets
- Bien récuprer le token sur sonar 
- Pour docker à bien vérfier que les noms des repos soit en lower case 

# Support
https://codefirst.iut.uca.fr/git/Templates_CodeFirst
